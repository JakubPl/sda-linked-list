package pl.sda;


import java.util.Arrays;

public class BubbleSortMain {
    public static void main(String[] args) {
        int sortMe[] = {112, 4, 8, 2, 6,20};
        bubbleSort(sortMe);

        System.out.println(Arrays.toString(sortMe));
    }

    private static void bubbleSort(int[] sortMe) {
        int n = sortMe.length;
        int ileRazyDominujaca = 0;
        do {
            for (int i = 0; i < n - 1; i++) {
                ileRazyDominujaca ++;
                if (sortMe[i] > sortMe[i + 1]) {
                    int tmp = sortMe[i];
                    sortMe[i] = sortMe[i + 1];
                    sortMe[i+1] = tmp;
                }
            }
            n = n - 1;
        } while (n > 1);

        System.out.println(ileRazyDominujaca);

    }
}
