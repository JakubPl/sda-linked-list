package pl.sda;

import java.util.*;

/**
 * Created by Jakub on 01.12.2018.
 */
public class BinarySystemChangeMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Integer toConvert = Integer.valueOf(scanner.nextLine());
        List<Integer> result = new ArrayList<>();
        while(toConvert > 0) {
            int moduloResult = toConvert % 2;
            toConvert = toConvert / 2;
            result.add(moduloResult);
        }
        Collections.reverse(result);
        //Math.pow(2, 2); - potegowanie

        result.forEach(System.out::println);
    }
}
