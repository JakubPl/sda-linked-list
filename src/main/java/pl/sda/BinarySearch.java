package pl.sda;

public class BinarySearch {
    public static void main(String[] args) {
        int[] tab = {2,5,7,20};
        int findMe = 7;
        int left = 0;
        int right = tab.length - 1;

        while (left < right) {
            int middle = (left + right) / 2;
            int middleValue = tab[middle];
            if(middleValue < findMe) {
                left = middle + 1;
            } else {
                right = middle;
            }
        }
        if(tab[left] == findMe) {
            System.out.println("Znaleziono na pozycji: " + left);
        } else {
            System.out.println("Nie znaleziono");
        }
    }
}
