package pl.sda;

public class StringListElement {
    private String value;
    private StringListElement nextElement;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public StringListElement next() {
        return nextElement;
    }

    public void setNextElement(StringListElement nextElement) {
        this.nextElement = nextElement;
    }
}
