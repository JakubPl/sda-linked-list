package pl.sda;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Jakub on 01.12.2018.
 */
public class SimpleSearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] split = scanner.nextLine().split(" ");
        String searchFor = scanner.nextLine();

        for (int i = 0; i < split.length; i++) {
            if(split[i].equals(searchFor)) {
                System.out.println("Znalazlem! Jest na pozycji: " + i);
                break;
            }
        }
    }
}
