package pl.sda;

public class DoubleLinkedStringList {
    private DoubleLinkedListStringElement first;
    private DoubleLinkedListStringElement last;
    private long size;

    public long size() {
        return size;
    }

    //pokazac jak dziala StringPool

    public DoubleLinkedListStringElement head() {
        return first;
    }

    public String get(long n) {
        DoubleLinkedListStringElement iter = this.first;
        String getOut = null;
        int i = 0;
        while (iter != null) {
            if (i == n) {
                getOut = iter.getValue();
                break;
            }
            i++;
            iter = iter.next();
        }
        return getOut;
    }

    public void add(String valueToAdd) {
        DoubleLinkedListStringElement newElement = new DoubleLinkedListStringElement();
        newElement.setValue(valueToAdd);
        if (size == 0) {
            first = newElement;
            last = newElement;
        } else {
            last.setNextElement(newElement);
            newElement.setPrevElement(last);
            last = newElement;
        }
        size++;
    }

    public void remove(String toRemove) {
        DoubleLinkedListStringElement actualElement = first;
        while (actualElement != null) {
            if (actualElement.getValue().equals(toRemove)) {
                if (actualElement == first) {
                    first = actualElement.next();
                    first.setPrevElement(null);
                } else if (actualElement == last) {
                    last = actualElement.prev();
                    last.setNextElement(null);
                } else {
                    DoubleLinkedListStringElement actualPrev = actualElement.prev();
                    DoubleLinkedListStringElement actualNext = actualElement.next();

                    actualPrev.setNextElement(actualNext);
                    actualNext.setPrevElement(actualPrev);
                }
                size--;
            }
            actualElement = actualElement.next();
        }
    }

    public DoubleLinkedListStringElement tail() {
        return last;
    }
}
