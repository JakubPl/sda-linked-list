package pl.sda;

public class DoubleLinkedListStringElement {
    private String value;
    private DoubleLinkedListStringElement nextElement;
    private DoubleLinkedListStringElement prevElement;

    public DoubleLinkedListStringElement prev() {
        return prevElement;
    }

    public void setPrevElement(DoubleLinkedListStringElement prevElement) {
        this.prevElement = prevElement;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DoubleLinkedListStringElement next() {
        return nextElement;
    }

    public void setNextElement(DoubleLinkedListStringElement nextElement) {
        this.nextElement = nextElement;
    }
}
