package pl.sda;

/**
 * Created by Jakub on 25.11.2018.
 */
public class AlgMain {
    public static void main(String[] args) {
        int suma = 0;
        int n = 2;
        int i = 0;
        int ileRazyWykonalaSieOperacjaDominujaca = 0;
        for(; i < n*2; i ++) {
            ileRazyWykonalaSieOperacjaDominujaca++;
            suma += i;
        }

        System.out.println("Operacja domominujaca " + ileRazyWykonalaSieOperacjaDominujaca);
        System.out.println(suma);
    }
}
