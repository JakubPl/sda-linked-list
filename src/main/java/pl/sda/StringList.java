package pl.sda;

public class StringList {
    private StringListElement first;
    private StringListElement last;
    private long size;

    public long size() {
        return size;
    }

    //pokazac jak dziala StringPool

    public StringListElement head() {
        return first;
    }

    public void remove(String toRemove) {
        StringListElement actualElement = first;
        StringListElement prevElement = null;
        while(actualElement != null) {
            if(actualElement.getValue().equals(toRemove)) {
                if(first == actualElement) {
                    //1->2->3->null
                    StringListElement secondElement = first.next();
                    first = secondElement;
                    //usuwamy pierwszy element listy
                    //czyli ustaw head na nastepnik elementu obecnego heada
                } else if(last == actualElement) {
                    last = prevElement;
                    prevElement.setNextElement(null);
                    //usuwamy ostatni element listy
                    //czyli ustaw poprzednikowi aktualnego elementu pole next na null
                    //ustaw nowy tail (pole last) na prevElement
                } else {
                    //jestesmy w srodku
                    //czyli usuwamy ktorys ze srodkowych elementow listy
                    //wiec ustaw wartosc pola next poprzednika na wartosc pola next aktualnego elementu
                    //przyklad:
                    //przed usunieciem: 1->2->3 i chce usunac 2
                    //po usunieciu:1->3
                    //               2 - niepolaczone z niczym

                    StringListElement nextElementOfActualElement = actualElement.next();
                    prevElement.setNextElement(nextElementOfActualElement);
                    actualElement.setNextElement(null);
                }
                size --;
            }

            prevElement = actualElement;
            actualElement = actualElement.next();
        }
    }

    public void add(String value) {
        StringListElement element = new StringListElement();
        element.setValue(value);
        if(size == 0) {
            first = element;
            last = element;
        } else {
            last.setNextElement(element);
            last = element;
        }
        size ++;
    }

    public String get(long index) {
        StringListElement iter = first;
        int currentIndex = 0;
        String getOut = null;
        while (iter != null) {
            if(currentIndex == index) {
                getOut = iter.getValue();
                break;
            }
            currentIndex ++;
            iter = iter.next();
        }
        return getOut;
    }

}
