package pl.sda;

import java.util.Arrays;

public class AnagramChecker {
    private final String word;

    public AnagramChecker(String word) {
        this.word = word;
    }

    public boolean test(String otherWord) {
        char[] wordArray = prepareToTest(word);
        char[] otherWordArray = prepareToTest(otherWord);
        Arrays.sort(wordArray);
        Arrays.sort(otherWordArray);
        return Arrays.equals(wordArray, otherWordArray);
    }

    private char[] prepareToTest(String someWord) {
        return someWord
                .toLowerCase()
                .trim()
                .toCharArray();
    }
}
