package pl.sda;


import java.util.Scanner;

public class DoubleLinkedListMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userInput = scanner.nextLine();

        String[] elementsToAddToList = userInput.split(" ");
        DoubleLinkedStringList myList = new DoubleLinkedStringList();


        for(String element : elementsToAddToList) {
            myList.add(element);
        }

        DoubleLinkedListStringElement reverseIter = myList.tail();
        while (reverseIter != null) {
            System.out.println(reverseIter.getValue());
            reverseIter = reverseIter.prev();
        }

    }
}
