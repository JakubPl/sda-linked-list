package pl.sda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Jakub on 01.12.2018.
 */
public class DecimalSystemChangeMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //IN: 1101
        String[] onesZerosArray = scanner.nextLine().split("");
        List<Integer> zeroOrOneList = Stream.of(onesZerosArray)
                .map(zeroOrOne -> Integer.valueOf(zeroOrOne))
                .collect(Collectors.toList());

        Collections.reverse(zeroOrOneList);
        //1011
        int decimalResult = 0;
        int i = 0;

        //1.1
        //2.0
        //3.4
        //4.8
        for(Integer element : zeroOrOneList) {
            decimalResult += element * Math.pow(2, i);
            i++;
        }

        System.out.println(decimalResult);

    }
}
